for i=1:28
	a = zeros(28,28);
	a(i,:)=1;
	b = reshape(a',784,1);
	train_h(i,:) = b;
end

for i=1:28
	a = zeros(28,28);
	a(:,i)=1;
	b = reshape(a',784,1);
	train_v(i,:) = b;
end

for i=1:56
	b = reshape(train_x(i,:),28,28);
	imshow(b')
	pause;
end

for i=1:28
	train_y(i,1) = 1;
	train_y(i,2) = 0;
end

for i=29:56
	train_y(i,1) = 0;
	train_y(i,2) = 1;
end


load hv.mat;

train_x = double(train_x) / 255;
test_x  = train_x;
train_y = double(train_y);
test_y  = double(train_y);

% normalize
[train_x, mu, sigma] = zscore(train_x);
test_x = normalize(test_x, mu, sigma);

%% ex1 vanilla neural net

rand('state',0)
nn = nnsetup([784 100 2]);
opts.numepochs =  100;   %  Number of full sweeps through data
opts.batchsize = 1;  %  Take a mean gradient step over this many samples
nn.weightPenaltyL2 = 1e-4;
nn.activation_function = 'sigm';    %  Sigmoid activation function
nn.learningRate = 0.1;                %  Sigm require a lower learning rate

[nn, L] = nntrain(nn, train_x, train_y, opts);

[er, bad] = nntest(nn, test_x, test_y);

assert(er < 0.08, 'Too big error');

